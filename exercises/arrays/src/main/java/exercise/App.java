package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] array) {
        int[] newArray = new int[array.length];
        int length = array.length - 1;
        for (int i = 0; i <= length; i++) {
            newArray[i] = array[length - i];
        }
        return newArray;
    }

    public static int mult(int[] array) {
        int result;
        if (array.length == 0) {
            result = 1;
        } else {
            result = array[0];
        }

        for (int i = 1; i < array.length; i++) {
            result = result * array[i];
        }
        return result;
    }
    // END
}
