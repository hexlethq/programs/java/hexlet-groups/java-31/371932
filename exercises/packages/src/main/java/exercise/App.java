// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;


class App {

    public static double[] getMidpointOfSegment(double[][] segment) {
        return new double[]{(segment[0][0] + segment[1][0]) / 2, (segment[0][1] + segment[1][1]) / 2};
    }

    public static double[][] reverse(double[][] segment) {
        double[] pointBegin = Segment
                .getEndPoint(segment)
                .clone();
        double[] pointEnd = Segment
                .getBeginPoint(segment)
                .clone();
        return new double[][]{pointBegin, pointEnd};
    }

    public static boolean isBelongToOneQuadrant(double[][] segment) {

        double xPointOne = segment[0][0];
        double xPointTwo = segment[1][0];
        double yPointOne = segment[0][1];
        double yPointTwo = segment[1][1];

        boolean firstQuadrant = (xPointOne > 0 && yPointOne > 0) && (xPointTwo > 0 && yPointTwo > 0);
        boolean secondQuadrant = (xPointOne < 0 && yPointOne > 0) && (xPointTwo < 0 && yPointTwo > 0);
        boolean thirdQuadrant = (xPointOne < 0 && yPointOne < 0) && (xPointTwo < 0 && yPointTwo < 0);
        boolean fourQuadrant = (xPointOne > 0 && yPointOne < 0) && (xPointTwo > 0 && yPointTwo < 0);

        if (firstQuadrant) {
            return true;
        } else if (secondQuadrant) {
            return true;
        } else if (thirdQuadrant) {
            return true;
        } else if (fourQuadrant) {
            return true;
        } else {
            return false;
        }

    }
}

// END
