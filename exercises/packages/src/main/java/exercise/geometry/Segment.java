// BEGIN
package exercise.geometry;

public class Segment {

    public static double[][] segment;

    public static double[][] makeSegment(double[] pointOne, double[] pointTwo) {
        return segment = new double[][]{pointOne, pointTwo};
    }

    public static double[] getBeginPoint(double[][] segment){
        return segment[0];
    }

    public static double[] getEndPoint(double[][] segment) {
        return segment[1];
    }
}
// END
