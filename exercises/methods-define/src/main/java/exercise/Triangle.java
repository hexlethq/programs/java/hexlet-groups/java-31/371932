package exercise;

class Triangle {
    // BEGIN
    final static double PI = 3.14;

    public static void main(String[] args) {

        System.out.println(getSquare(4, 5, 45));

    }

    public static double getSquare(int firstSide, int secondSide, int corner) {
        return (double) (firstSide*secondSide)/2 * Math.sin(corner*PI/180);

    }
    // END
}
