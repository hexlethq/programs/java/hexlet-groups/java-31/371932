package exercise;

class Converter {
    // BEGIN
    final static int CONVERT_SIZE = 1024;

    public static void main(String[] args) {
        String convertTrend = "b";
        int kilobyteNum = 10;
        System.out.println("10 Kb = " + convert(kilobyteNum, convertTrend) + " b");
    }

    public static int convert(int num, String str) {
        int result;

        switch (str) {
            case "b":
                result = num * CONVERT_SIZE;
                break;
            case "Kb":
                result = num / CONVERT_SIZE;
                break;
            default:
                result = 0;
                break;
        }
        return result;
    }

    // END
}

