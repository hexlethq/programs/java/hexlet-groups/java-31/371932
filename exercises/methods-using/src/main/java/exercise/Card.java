package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String result = "";
        for (int i = 0; i < starsCount ; i++) {
            result += "*";
        }
        System.out.println(result+cardNumber.substring(12, 16));
        // END
    }
}
