package exercise;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] array) {
        int index = 0;
        int count = 0;
        if (array.length == 0) {
            index = -1;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 0) {
                index = array[i] > array[index]
                        ? i
                        : index;
            } else if (array[i] > 0) {
                count++;
            }
        }

        if (count == array.length) {
            index = -1;
        }

        return index;
    }

    public static int[] getElementsLessAverage(int[] array) {
        return array.length == 0
                ? new int[0]
                : receiveLessAverageElementsArray(array);

    }

    private static int[] receiveLessAverageElementsArray(int[] array) {
        int[] newArray;
        int arraySum = 0;
        int arithmetic;
        int count = 0;
        for (int i : array) {
            arraySum += i;
        }
        arithmetic = arraySum / array.length;

        for (int i : array) {
            if (i <= arithmetic) {
                count++;
            }
        }
        newArray = new int[count];
        for (int i = 0; i < count; i++) {
            newArray[i] = array[i] <= arithmetic ? array[i] : 0;
        }
        return newArray;
    }
    // END

    public static void main(String[] args) {

        int[] array = {8, -2, 5, 0, 1};
        System.out.println(getSumBeforeMinAndMax(array));
        int[] newArray = {-3, 6, 0, 2, 10};
        System.out.println(getSumBeforeMinAndMax(newArray));
    }

    public static int getSumBeforeMinAndMax(int[] array) {
        int sum = 0;
        int min = array[0];
        int max = min;
        int indexMIN = 0;
        int indexMAX = 0;
        for (int i : array) {
            min = Math.min(min, i);
            max = Math.max(max, i);
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] == min) {
                indexMIN = i;
            } else if (array[i] == max) {
                indexMAX = i;
            }
        }
        if (indexMIN < indexMAX) {
            if (indexMIN + 1 == indexMAX) {
                return 0;
            }
            for (int i = indexMIN + 1; i < indexMAX; i++) {
                sum += array[i];
            }
        }
        if (indexMAX < indexMIN) {
            if (indexMAX + 1 == indexMIN) {
                return sum = 0;
            }
            for (int i = indexMAX + 1; i < indexMIN; i++) {
                sum += array[i];
            }
        }

        return sum;
    }
}


