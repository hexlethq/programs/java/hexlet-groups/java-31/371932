package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        return new int[]{x, y};
    }

    public static int getX(int[] point) {
        return point[0];
    }

    public static int getY(int[] point) {
        return point[1];
    }

    public static String pointToString(int[] point) {
        return "(" + getX(point) + ", " + getY(point) + ")";
    }

    public static int getQuadrant(int[] point) {
        int quadrant = 0;
        if (getX(point) > 0 && getY(point) > 0) {
            quadrant = 1;
        } else if (getX(point) < 0 && getY(point) > 0) {
            quadrant = 2;
        } else if (getX(point) < 0 && getY(point) < 0) {
            quadrant = 3;
        } else if (getX(point) > 0 && getY(point) < 0) {
            quadrant = 4;
        } else if (getX(point) == 0 && getY(point) == 0) {
            return quadrant;
        }

        return quadrant;
    }
    // END

    public static int[] getSymmetricalPointByX(int[] point){

        for (int i = 0; i < point.length; i++) {
            if (point[i] == getY(point)) {
                point[i] *= -1;
            }
        }
        return new int[] {getX(point), getY(point)};
    }

    public static int calculateDistance(int[] firstPoint, int[] secondPoint) {
        return (int) Math.sqrt(((getX(secondPoint)-getX(firstPoint))*((getX(secondPoint)-getX(firstPoint)))
                + ((getY(secondPoint)-getY(firstPoint))*(getY(secondPoint)-getY(firstPoint)))));
    }

}
