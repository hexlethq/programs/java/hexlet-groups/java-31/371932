package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        char letter = str.charAt(0);
        String abbrev = Character.toString(letter);
        if (abbrev.equals(" ")) {
            abbrev = "";
        }
        for (int i = 0; i < str.length() - 1; i++) {
            if (str.charAt(i) == ' ' && Character.isLetter(str.charAt(i + 1))) {
                letter = str.charAt(i + 1);
                abbrev = abbrev + Character.toUpperCase(letter);
            }
        }
        return abbrev;
    }
    // END
}
