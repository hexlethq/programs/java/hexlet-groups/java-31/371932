package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] sort(int[] array) {
        int tmp;
        for (int i = 0; i < array.length; i++) {
            for (int j = i; j < array.length; j++) {
                if(array[i]>array[j]){
                    tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                }
            }
        }
        return array;
    }

    public static void main(String[] args) {
        int[] array = {9, 5, 0, 11, 2, 7, 1, -6};
        System.out.println(Arrays.toString(selectSort(array)));

    }
    public static int[] selectSort(int[] array){
        int index, tmp;
        for (int i = 0; i < array.length; i++) {
            index = i;
            for (int j = index; j < array.length; j++) {
                if(array[j]<array[index]){
                    index = j;
                }
            }
            tmp = array[i];
            array[i] = array[index];
            array[index] = tmp;
        }
        return array;
    }
    // END
}
