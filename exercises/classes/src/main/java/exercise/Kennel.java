package exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// BEGIN
public class Kennel {

    private static String[][] puppiesArr = new String[0][];

    private Kennel() {
    }

    public static void addPuppy(String[] puppy) {

        String[][] tmp = new String[puppiesArr.length + 1][];
        tmp[0] = puppy;
        for (int i = 1; i <= puppiesArr.length; i++) {
            tmp[i] = puppiesArr[i - 1];
        }
        puppiesArr = tmp;
    }

    public static void addSomePuppies(String[][] puppies) {

        String[][] tmp = new String[puppiesArr.length + puppies.length][];
        int index = 0;
        for (int i = 0; i < tmp.length; i++) {
            if (i < puppiesArr.length) {
                tmp[i] = puppiesArr[i];
            }
            if (i >= puppiesArr.length) {
                tmp[i] = puppies[index];
                index++;
            }
        }
        puppiesArr = tmp;
    }

    public static int getPuppyCount() {
        return puppiesArr.length;
    }

    public static boolean isContainPuppy(String puppyName) {
        for (String[] strings : puppiesArr) {
            if (puppyName.equals(strings[0])) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return puppiesArr;
    }

    public static String[] getNamesByBreed(String breed) {
        int count = 0;
        for (String[] strings : puppiesArr) {
            if (strings[1].equals(breed)) {
                count++;
            }
        }

        String[] allNames = new String[count];
        count = 0;
        for (String[] strings : puppiesArr) {
            if (strings[1].equals(breed)) {
                allNames[count] = strings[0];
                count++;
            }
        }

        return allNames;
    }

    public static void resetKennel() {

        puppiesArr = new String[0][];
    }

    public static boolean removePuppy(String puppyName) {
        for (int i = 0; i < puppiesArr.length; i++) {
            if (puppiesArr[i][0].equals(puppyName)) {
                String[][] tmp = new String[puppiesArr.length - 1][];
                int index = 0;
                for (int j = 0; j <= tmp.length; j++) {
                    if (j == i) {
                        continue;
                    }
                    tmp[index] = puppiesArr[j];
                    index++;
                }
                puppiesArr = tmp;
                return true;
            }
        }
        return false;
    }
}

/*
    private static ArrayList<String[]> puppyArray = new ArrayList<>();

    private Kennel() {
    }

    public static void addPuppy(String[] puppy) {

        puppyArray.add(puppy);
    }

    public static void addSomePuppies(String[][] puppiesArr) {

        puppyArray.addAll(Arrays.asList(puppiesArr));
    }

    public static int getPuppyCount() {
        return puppyArray.size();
    }

    public static boolean isContainPuppy(String puppyName) {
        for (String[] strings : puppyArray) {
            if (puppyName.equals(strings[0])) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        String[][] puppies = new String[puppyArray.size()][];
        puppyArray.toArray(puppies);
        return puppies;
    }

    public static String[] getNamesByBreed(String breed) {
        List<String> breeds = new ArrayList<>();
        for (String[] strings : puppyArray) {
            if (strings[1].equals(breed)) {
                breeds.add(strings[0]);
            }
        }

        String[] allNames = new String[breeds.size()];
        breeds.toArray(allNames);
        return allNames;
    }

    public static void resetKennel() {
        puppyArray.clear();
    }

    public static boolean removePuppy(String puppyName) {
        for (int i = 0; i < puppyArray.size(); i++) {
            if (puppyArray.get(i)[0].equals(puppyName)) {
                puppyArray.remove(i);
                return true;
            }
        }
        return false;
    }
}

*/

// END
