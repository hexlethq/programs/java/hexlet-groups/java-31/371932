package exercise;

class App {

    // BEGIN
    public static String getTypeOfTriangle(int sideA, int sideB, int sideC) {
        String result = null;
        if ((sideA + sideB < sideC) || sideB + sideC < sideA || sideA + sideC < sideB) {
            result = "Треугольник не существует";
        } else if (sideA != sideB && sideA != sideC && sideB != sideC) {
            result = "Разносторонний";
        }
        else if (sideA == sideB && sideA == sideC ) {
            result = "Равносторонний";
        }
        else if (sideA == sideB || sideA == sideC) {
            result = "Равнобедренный";
        }
        return result;
    }
    // END
}
