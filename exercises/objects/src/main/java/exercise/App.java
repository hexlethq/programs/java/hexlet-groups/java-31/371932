package exercise;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class App {

    // BEGIN
    final static String OPEN_UL = "<ul>";
    final static String CLOSE_UL = "</ul>";
    final static String OPEN_LI = "<li>";
    final static String CLOSE_LI = "</li>";
    final static String SPACE = "  ";
    final static String NEW_LINE = "\n";

    public static String buildList(String[] arrayStrings) {
        return arrayStrings.length == 0
                ? ""
                : makeHTMList(arrayStrings);
    }

    private static String makeHTMList(String[] arrayStrings) {
        StringBuilder sb = new StringBuilder();
        sb.append(OPEN_UL)
                .append(NEW_LINE)
                .append(SPACE)
                .append(OPEN_LI)
                .append(arrayStrings[0])
                .append(CLOSE_LI)
                .append(NEW_LINE);

        for (int i = 1; i < arrayStrings.length - 1; i++) {
            sb.append(SPACE)
                    .append(OPEN_LI)
                    .append(arrayStrings[i])
                    .append(CLOSE_LI)
                    .append(NEW_LINE);

        }
        sb.append(SPACE)
                .append(OPEN_LI)
                .append(arrayStrings[arrayStrings.length - 1])
                .append(CLOSE_LI)
                .append(NEW_LINE)
                .append(CLOSE_UL);

        return sb.toString();
    }

    public static String getUsersByYear(String[][] strings, int year) {
        if (strings == null || year < 0) {
            System.out.println("getUsersByYear: Unexpected conditions");
            return "";
        }

        StringBuilder sb = new StringBuilder();

        for (String[] str : strings) {
            if (year == Integer.parseInt(str[1].substring(0, 4))) {
                if (sb.length() == 0) {
                    sb.insert(0, OPEN_UL + NEW_LINE);
                }
                sb.append(SPACE)
                        .append(OPEN_LI)
                        .append(str[0])
                        .append(CLOSE_LI)
                        .append(NEW_LINE);
            }
        }

        return sb.length() == 0
                ? ""
                : sb.append(CLOSE_UL).toString();
    }

    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date)
            throws Exception {
        // BEGIN
        Date departureDate = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH).parse(date);
        SimpleDateFormat sdfSTR = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        Date youngestDate = null;
        String youngestUser = "";
        Date tmpDate;

        for (String[] user : users) {
            tmpDate = sdfSTR.parse(user[1]);
            if (departureDate.after(tmpDate)) {

                if (null == youngestDate) {
                    youngestDate = tmpDate;
                    youngestUser = user[0];
                } else if (youngestDate.before(tmpDate)) {
                    youngestDate = tmpDate;
                    youngestUser = user[0];
                }
            }
        }
        return youngestUser;
    }
    // END
}

